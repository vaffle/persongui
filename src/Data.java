/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persondb;

import java.util.ArrayList;
import java.util.Scanner;
import static persondb.Data.persons;

/**
 *
 * @author informatics
 */
public class Data {
    static Scanner k = new Scanner(System.in);
    private static String Username,Password,Firstname,Lastname;
    private static int Age;
    
    static ArrayList<Person>persons = new ArrayList<Person>();
    static void load() {
        persons.add(new Person("testuser1","111111","name1","lastname1",21));
        persons.add(new Person("testuser2","222222","name2","lastname2",22));
        persons.add(new Person("testuser3","333333","name3","lastname3",23));
        persons.add(new Person("testuser4","444444","name4","lastname4",24));
        persons.add(new Person("testuser5","555555","name5","lastname5",25));
    }

    static boolean isUserPasswordCorrct(String username, String password) {
        if(username.equals("admin") && password.equals("admin"))return true;
        for(Person p: persons){
            if(p.getUsername().equals(username)){
                if(p.getPassword().equals(password)){
                     return true;
                }
            }
        }     
        return false;
    }
    
    
    static void add() {
        while(true){
            System.out.print("Username:");
            Username=k.next();
            if(checkUser(Username)){
                break;
            }
           continue;
        }
        System.out.print("Password:");
        Password=k.next();
        System.out.print("Firstname:");
        Firstname=k.next();
        System.out.print("Lastname:");
        Lastname=k.next();
        System.out.print("Age:");
        Age=k.nextInt();
        
        persons.add(new Person(Username,Password,Firstname,Lastname,Age));

    }

    private static boolean checkUser(String Username) {
        for(Person p: persons){
            if(p.getUsername().equals(Username)){
                System.out.println("username is already taken");
                     return false;
            }
        } 
        return true;
    }
}
